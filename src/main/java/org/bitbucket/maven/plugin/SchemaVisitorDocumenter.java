package org.bitbucket.maven.plugin;

import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Stack;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSAttGroupDecl;
import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSIdentityConstraint;
import com.sun.xml.xsom.XSListSimpleType;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSNotation;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.XSUnionSimpleType;
import com.sun.xml.xsom.XSWildcard;
import com.sun.xml.xsom.XSWildcard.Any;
import com.sun.xml.xsom.XSWildcard.Other;
import com.sun.xml.xsom.XSWildcard.Union;
import com.sun.xml.xsom.XSXPath;
import com.sun.xml.xsom.impl.Const;
import com.sun.xml.xsom.visitor.XSSimpleTypeVisitor;
import com.sun.xml.xsom.visitor.XSTermVisitor;
import com.sun.xml.xsom.visitor.XSVisitor;
import com.sun.xml.xsom.visitor.XSWildcardFunction;

public class SchemaVisitorDocumenter implements XSVisitor, XSSimpleTypeVisitor {

	private Stack<PathComponent> path = new Stack<PathComponent>();

	public static final String XPATH_PATH_SEPERATOR = "/";

	public static final String XPATH_VALUE_SEPERATOR = "@";

	/** output is sent to this object. */
	// private final Writer out;

	private final Document document;

	private Element root;

	private Element currentElement;

	public SchemaVisitorDocumenter(Document _document) {
		this.document = _document;
		this.root = this.document.getDocumentElement();
	}

	/** If IOException is encountered, this flag is set to true. */
	private boolean hadError = false;

	/** Flush the stream and check its error state. */
	public boolean checkError() {
		return hadError;
	}

	public void visit(XSSchemaSet s) {
		Iterator<XSSchema> itr = s.iterateSchema();
		while (itr.hasNext()) {
			schema(itr.next());
		}
	}

	public void schema(XSSchema schema) {
		if (schema.getTargetNamespace().equals(Const.schemaNamespace)) {
			return;
		}

		Iterator<XSElementDecl> itrXSElementDecl = schema.iterateElementDecls();
		while (itrXSElementDecl.hasNext()) {
			elementDecl(itrXSElementDecl.next());
		}
	}

	public void attGroupDecl(XSAttGroupDecl decl) {
		Iterator<? extends XSAttGroupDecl> itrXSAttGroupDecl = decl
				.iterateAttGroups();

		while (itrXSAttGroupDecl.hasNext()) {
			XSAttGroupDecl attGroupDecl = itrXSAttGroupDecl.next();
			attGroupDecl.visit(this);
		}

		Iterator<? extends XSAttributeUse> itrXSAttributeUse = decl
				.iterateDeclaredAttributeUses();
		while (itrXSAttributeUse.hasNext()) {
			attributeUse(itrXSAttributeUse.next());
		}
	}

	public void attributeUse(XSAttributeUse use) {
		XSAttributeDecl decl = use.getDecl();

		documentXSAttributeUse(use);

		if (use.isRequired()) {
		}

		if (use.getFixedValue() != null
				&& use.getDecl().getFixedValue() == null) {

		}
		if (use.getDefaultValue() != null
				&& use.getDecl().getDefaultValue() == null) {

		}
		if (decl.isLocal()) {
			// this is anonymous attribute use
			dump(decl);
		} else {
			// reference to a global one
		}
	}

	private void documentXSAttributeUse(XSAttributeUse use) {
		if (currentElement != null) {
			if (use.isRequired()) {
				currentElement.setAttribute("required", "true");
			} else {
				currentElement.setAttribute("required", "false");
			}
		}
	}

	public void attributeDecl(XSAttributeDecl decl) {
		dump(decl);
	}

	private void dump(XSAttributeDecl decl) {
		XSSimpleType type = decl.getType();

		documentXSAttributeDecl(decl);

		if (type.isLocal()) {
			simpleType(type);
		}
	}

	public void simpleType(XSSimpleType type) {
		type.visit((XSSimpleTypeVisitor) this);
	}

	public void listSimpleType(XSListSimpleType type) {
		XSSimpleType itemType = type.getItemType();

		if (itemType.isLocal()) {
			simpleType(itemType);
		} else {
			// global type
		}
	}

	public void unionSimpleType(XSUnionSimpleType type) {
		final int len = type.getMemberSize();

		documentXsUnionSimpleType(type);

		for (int i = 0; i < len; i++) {
			XSSimpleType member = type.getMember(i);
		}

		for (int i = 0; i < len; i++) {
			XSSimpleType member = type.getMember(i);
			if (member.isLocal()) {
				simpleType(member);
			}
		}
	}

	private void documentXsUnionSimpleType(XSUnionSimpleType type) {
		this.currentElement.setAttribute("type", "union");
	}

	public void restrictionSimpleType(XSRestrictionSimpleType type) {

		if (type.getBaseType() == null) {
			// don't print anySimpleType
			if (!type.getName().equals("anySimpleType")) {
				throw new InternalError();
			}
			if (!Const.schemaNamespace.equals(type.getTargetNamespace())) {
				throw new InternalError();
			}
			return;
		}

		XSSimpleType baseType = type.getSimpleBaseType();

		if (baseType.isLocal()) {
			simpleType(baseType);
		}

		Iterator<XSFacet> itrXSFacet = type.iterateDeclaredFacets();
		while (itrXSFacet.hasNext()) {
			facet(itrXSFacet.next());
		}

	}

	public void complexType(XSComplexType type) {
		if (type.getContentType().asSimpleType() != null) {
			// simple content
			XSType baseType = type.getBaseType();

			if (type.getDerivationMethod() == XSType.RESTRICTION) {
				// restriction
				dumpComplexTypeAttribute(type);
			} else {
				// extension
				if (type.isGlobal()
						&& type.getTargetNamespace().equals(
								baseType.getTargetNamespace())
						&& type.getName().equals(baseType.getName())) {
					baseType.visit(this);
				}
				dumpComplexTypeAttribute(type);
			}

		} else {
			// complex content
			XSComplexType baseType = type.getBaseType().asComplexType();

			if (type.getDerivationMethod() == XSType.RESTRICTION) {
				// restriction
				type.getContentType().visit(this);

				if (type.getContentType().asEmpty() != null
						&& type.getAttGroups().isEmpty()
						&& type.getAttributeUses().isEmpty()) {
					documentXSComplexType(type);
				}
				dumpComplexTypeAttribute(type);
			} else {
				// extension
				// if (type.isGlobal() &&
				// type.getTargetNamespace().equals(baseType.getTargetNamespace())
				// && type.getName().equals(baseType.getName())) {
				// if
				// (type.getTargetNamespace().equals(baseType.getTargetNamespace()))
				// {
				// baseType.visit(this);
				// }

				type.getExplicitContent().visit(this);
				baseType.visit(this);
				dumpComplexTypeAttribute(type);
			}
		}
	}

	private void dumpComplexTypeAttribute(XSComplexType type) {
		Iterator<? extends XSAttGroupDecl> itrXSAttGroupDecl = type
				.iterateAttGroups();

		while (itrXSAttGroupDecl.hasNext()) {
			XSAttGroupDecl attGroupDecl = itrXSAttGroupDecl.next();
			attGroupDecl.visit(this);
		}

		Iterator<? extends XSAttributeUse> itrXSAttributeUse = type
				.iterateDeclaredAttributeUses();
		while (itrXSAttributeUse.hasNext()) {
			XSAttributeUse attributeUse = itrXSAttributeUse.next();
			attributeUse(attributeUse);
		}

		XSWildcard awc = type.getAttributeWildcard();
		if (awc != null) {
			wildcard("anyAttribute", awc);
		}
	}

	public void elementDecl(XSElementDecl decl) {

		if (decl.getName().equals("CarService")) {
			System.out.println(decl.toString());

		}

		XSType type = decl.getType();
		if (type.isLocal()) {
			path.push(new PathComponent(decl.getName()));
			if (type.isLocal()) {
				type.visit(this);
			}
			path.pop();
		} else {
			path.push(new PathComponent(decl.getName()));
			if (type.isSimpleType()) {
				documentXSElementDecl(decl);
			} else {
				type.visit(this);
			}
			path.pop();
		}
	}

	public void modelGroupDecl(XSModelGroupDecl decl) {
		modelGroup(decl.getModelGroup());
	}

	public void modelGroup(XSModelGroup group) {
		final int len = group.getSize();
		for (int i = 0; i < len; i++) {
			particle(group.getChild(i));
		}
	}

	public void particle(XSParticle part) {
		documentXSParticle(part);

		BigInteger maxOccurs = part.getMaxOccurs();
		if (maxOccurs.intValue() == XSParticle.UNBOUNDED) {
		} else if (maxOccurs.intValue() != 1) {
		}

		BigInteger minOccurs = part.getMinOccurs();

		if (minOccurs.intValue() != 1) {
		}

		part.getTerm().visit(new XSTermVisitor() {

			public void elementDecl(XSElementDecl decl) {
				if (decl.isLocal()) {
					SchemaVisitorDocumenter.this.elementDecl(decl);
				} else {
				}
			}

			public void modelGroupDecl(XSModelGroupDecl decl) {
				// reference
			}

			public void modelGroup(XSModelGroup group) {
				SchemaVisitorDocumenter.this.modelGroup(group);
			}

			public void wildcard(XSWildcard wc) {
				SchemaVisitorDocumenter.this.wildcard("any", wc);
			}
		});
	}

	public void documentXSParticle(XSParticle part) {
		// int i;
		// if (currentElement != null) {
		// i = part.getMaxOccurs();
		// if (i == XSParticle.UNBOUNDED) {
		// currentElement.setAttribute("maxOccurs", "unbounded");
		// }
		// else if (i != 1) {
		// currentElement.setAttribute("maxOccurs", String.valueOf(i));
		// }
		// else {
		// currentElement.setAttribute("maxOccurs", "");
		// }
		//
		// i = part.getMinOccurs();
		//
		// if (i != 1) {
		// currentElement.setAttribute("minOccurs", String.valueOf(i));
		// }
		// else {
		// currentElement.setAttribute("minOccurs", "");
		//
		// }
		// }
	}

	public void wildcard(XSWildcard wc) {
		wildcard("any", wc);
	}

	private void wildcard(String tagName, XSWildcard wc) {
		switch (wc.getMode()) {
		case XSWildcard.LAX:
			break;
		case XSWildcard.STRTICT:
			break;
		case XSWildcard.SKIP:
			break;
		default:
			throw new AssertionError();
		}
	}

	private static final XSWildcardFunction<String> WILDCARD_NS = new XSWildcardFunction<String>() {

		public String any(Any wc) {
			return ""; // default
		}

		public String other(Other wc) {
			return " namespace='##other'";
		}

		public String union(Union wc) {
			StringBuffer buf = new StringBuffer(" namespace='");
			boolean first = true;
			for (String s : wc.getNamespaces()) {
				if (first) {
					first = false;
				} else {
					buf.append(' ');
				}
				buf.append(s);
			}
			return buf.append('\'').toString();
		}
	};

	public void annotation(XSAnnotation ann) {
	}

	public void identityConstraint(XSIdentityConstraint decl) {
	}

	public void xpath(XSXPath xp) {
	}

	public void empty(XSContentType t) {

	}

	public void facet(XSFacet facet) {
		documentXSFacet(facet);
	}

	public void notation(XSNotation notation) {
	}

	private void documentXSComplexType(XSComplexType type) {
		try {
			Element el = this.document
					.createElement(DocumentBuilderTagEnum.ELEMENT.getTag());
			this.currentElement = el;
			el.setAttribute("name", buildPath() + type.getName());
			this.root.appendChild(el);

		} catch (Exception e) {
		}
	}

	private void documentXSFacet(XSFacet facet) {
		this.currentElement.setAttribute("type", facet.getName());
	}

	private void documentXSAttributeDecl(XSAttributeDecl decl) {
		XSSimpleType type = decl.getType();
		try {
			Element el = this.document
					.createElement(DocumentBuilderTagEnum.ELEMENT.getTag());
			this.currentElement = el;
			el.setAttribute("name", buildPathWithValue(decl.getName()));
			if (!type.isLocal()) {
				el.setAttribute(
						"type",
						MessageFormat.format("'{'{0}'}'{1}",
								type.getTargetNamespace(), type.getName()));
			} else {
				type.visit((XSSimpleTypeVisitor) this);
			}

			this.root.appendChild(el);
		} catch (Exception e) {
		}
	}

	private String buildPath() {
		Iterator<PathComponent> pathIt = path.iterator();
		StringBuffer strbuf = new StringBuffer();
		while (pathIt.hasNext()) {
			strbuf.append(pathIt.next().getValue());
			strbuf.append(XPATH_PATH_SEPERATOR);
		}
		return strbuf.toString();
	}

	private String buildPathWithValue(String value) {
		StringBuffer strbuf = new StringBuffer(buildPath());
		strbuf.append(XPATH_VALUE_SEPERATOR);
		strbuf.append(value);
		return strbuf.toString();
	}

	private void documentXSElementDecl(XSElementDecl decl) {
		XSType type = decl.getType();

		try {
			Element el = this.document
					.createElement(DocumentBuilderTagEnum.ELEMENT.getTag());
			this.currentElement = el;
			el.setAttribute("name", buildPathWithValue(decl.getName()));
			if (!type.isLocal()) {
				el.setAttribute(
						"type",
						MessageFormat.format("'{'{0}'}'{1}",
								type.getTargetNamespace(), type.getName()));
			} else {
				type.visit((XSVisitor) this);
			}
			this.root.appendChild(el);

		} catch (Exception e) {
		}
	}
}
