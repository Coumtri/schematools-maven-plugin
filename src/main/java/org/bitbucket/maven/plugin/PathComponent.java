package org.bitbucket.maven.plugin;

public class PathComponent {
	private String value;

	public PathComponent(String value) {
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
