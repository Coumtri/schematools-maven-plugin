package org.bitbucket.maven.plugin;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Stack;

import com.sun.xml.xsom.XSAnnotation;
import com.sun.xml.xsom.XSAttGroupDecl;
import com.sun.xml.xsom.XSAttributeDecl;
import com.sun.xml.xsom.XSAttributeUse;
import com.sun.xml.xsom.XSComplexType;
import com.sun.xml.xsom.XSContentType;
import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSIdentityConstraint;
import com.sun.xml.xsom.XSListSimpleType;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSNotation;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSType;
import com.sun.xml.xsom.XSUnionSimpleType;
import com.sun.xml.xsom.XSWildcard;
import com.sun.xml.xsom.XSXPath;
import com.sun.xml.xsom.XSWildcard.Any;
import com.sun.xml.xsom.XSWildcard.Other;
import com.sun.xml.xsom.XSWildcard.Union;
import com.sun.xml.xsom.impl.Const;
import com.sun.xml.xsom.visitor.XSSimpleTypeVisitor;
import com.sun.xml.xsom.visitor.XSTermVisitor;
import com.sun.xml.xsom.visitor.XSVisitor;
import com.sun.xml.xsom.visitor.XSWildcardFunction;

public class SchemaVisitorWriter implements XSVisitor, XSSimpleTypeVisitor {

	private Stack<PathComponent> path = new Stack<PathComponent>();

	/** output is sent to this object. */
	private final Writer out;

	public SchemaVisitorWriter(Writer _out) {
		this.out = _out;
	}

	/** If IOException is encountered, this flag is set to true. */
	private boolean hadError = false;

	/** Flush the stream and check its error state. */
	public boolean checkError() {
		try {
			out.flush();
		} catch (IOException e) {
			hadError = true;
		}
		return hadError;
	}

	public void visit(XSSchemaSet s) {
		Iterator<XSSchema> itr = s.iterateSchema();
		while (itr.hasNext()) {
			schema(itr.next());
		}
	}

	public void schema(XSSchema schema) {
		if (schema.getTargetNamespace().equals(Const.schemaNamespace))
			return;

		Iterator<XSElementDecl> itrXSElementDecl = schema.iterateElementDecls();
		while (itrXSElementDecl.hasNext())
			elementDecl(itrXSElementDecl.next());
	}

	public void attGroupDecl(XSAttGroupDecl decl) {
		Iterator<? extends XSAttGroupDecl> itrXSAttGroupDecl = decl
				.iterateAttGroups();
		while (itrXSAttGroupDecl.hasNext()) {
			XSAttGroupDecl attGroupDecl = itrXSAttGroupDecl.next();
			attGroupDecl.visit(this);
		}

		Iterator<? extends XSAttributeUse> itrXSAttributeUse = decl
				.iterateDeclaredAttributeUses();
		while (itrXSAttributeUse.hasNext())
			attributeUse(itrXSAttributeUse.next());
	}

	public void attributeUse(XSAttributeUse use) {
		XSAttributeDecl decl = use.getDecl();

		String additionalAtts = "";

		if (use.isRequired())
			additionalAtts += " use=\"required\"";
		if (use.getFixedValue() != null
				&& use.getDecl().getFixedValue() == null)
			additionalAtts += " fixed=\"" + use.getFixedValue() + '\"';
		if (use.getDefaultValue() != null
				&& use.getDecl().getDefaultValue() == null)
			additionalAtts += " default=\"" + use.getDefaultValue() + '\"';

		if (decl.isLocal()) {
			// this is anonymous attribute use
			dump(decl, additionalAtts);
		} else {
			// reference to a global one
		}
	}

	public void attributeDecl(XSAttributeDecl decl) {
		dump(decl, "");
	}

	private void dump(XSAttributeDecl decl, String additionalAtts) {
		XSSimpleType type = decl.getType();

		outputXSAttributeDecl(decl);

		if (type.isLocal()) {
			simpleType(type);
		}

	}

	public void simpleType(XSSimpleType type) {
		type.visit((XSSimpleTypeVisitor) this);
	}

	public void listSimpleType(XSListSimpleType type) {
		XSSimpleType itemType = type.getItemType();

		if (itemType.isLocal()) {
			simpleType(itemType);
		} else {
			// global type
		}
	}

	public void unionSimpleType(XSUnionSimpleType type) {
		final int len = type.getMemberSize();
		StringBuffer ref = new StringBuffer();

		for (int i = 0; i < len; i++) {
			XSSimpleType member = type.getMember(i);
			if (member.isGlobal())
				ref.append(MessageFormat.format(" '{'{0}'}'{1}",
						member.getTargetNamespace(), member.getName()));
		}

		for (int i = 0; i < len; i++) {
			XSSimpleType member = type.getMember(i);
			if (member.isLocal())
				simpleType(member);
		}
	}

	public void restrictionSimpleType(XSRestrictionSimpleType type) {

		if (type.getBaseType() == null) {
			// don't print anySimpleType
			if (!type.getName().equals("anySimpleType"))
				throw new InternalError();
			if (!Const.schemaNamespace.equals(type.getTargetNamespace()))
				throw new InternalError();
			return;
		}

		XSSimpleType baseType = type.getSimpleBaseType();

		if (baseType.isLocal())
			simpleType(baseType);

		Iterator<XSFacet> itrXSFacet = type.iterateDeclaredFacets();
		while (itrXSFacet.hasNext())
			facet(itrXSFacet.next());
	}

	public void complexType(XSComplexType type) {
		if (type.getContentType().asSimpleType() != null) {
			// simple content
			XSType baseType = type.getBaseType();

			if (type.getDerivationMethod() == XSType.RESTRICTION) {
				// restriction
				dumpComplexTypeAttribute(type);
			} else {
				// extension
				if (type.isGlobal()
						&& type.getTargetNamespace().equals(
								baseType.getTargetNamespace())
						&& type.getName().equals(baseType.getName())) {
					baseType.visit(this);
				}
				dumpComplexTypeAttribute(type);
			}

		} else {
			// complex content
			XSComplexType baseType = type.getBaseType().asComplexType();

			if (type.getDerivationMethod() == XSType.RESTRICTION) {
				// restriction
				type.getContentType().visit(this);

				if (type.getContentType().asEmpty() != null
						&& type.getAttGroups().isEmpty()
						&& type.getAttributeUses().isEmpty()) {
					outputXSComplexType(type);
				}
				dumpComplexTypeAttribute(type);
			} else {
				// extension
				// if (type.isGlobal() &&
				// type.getTargetNamespace().equals(baseType.getTargetNamespace())
				// && type.getName().equals(baseType.getName())) {
				if (type.getTargetNamespace().equals(
						baseType.getTargetNamespace())) {
					baseType.visit(this);
				}

				type.getExplicitContent().visit(this);
				dumpComplexTypeAttribute(type);

			}

		}

	}

	private void dumpComplexTypeAttribute(XSComplexType type) {
		Iterator<? extends XSAttGroupDecl> itrXSAttGroupDecl = type
				.iterateAttGroups();
		while (itrXSAttGroupDecl.hasNext()) {
			XSAttGroupDecl attGroupDecl = itrXSAttGroupDecl.next();
			attGroupDecl.visit(this);
		}

		Iterator<? extends XSAttributeUse> itrXSAttributeUse = type
				.iterateDeclaredAttributeUses();
		while (itrXSAttributeUse.hasNext()) {
			XSAttributeUse attributeUse = itrXSAttributeUse.next();
			attributeUse(attributeUse);
		}

		XSWildcard awc = type.getAttributeWildcard();
		if (awc != null)
			wildcard("anyAttribute", awc, "");
	}

	public void elementDecl(XSElementDecl decl) {
		elementDecl(decl, "");
	}

	private void elementDecl(XSElementDecl decl, String extraAtts) {
		XSType type = decl.getType();
		if (type.isLocal()) {
			path.push(new PathComponent(decl.getName()));
			if (type.isLocal())
				type.visit(this);
			path.pop();
		} else {
			path.push(new PathComponent(decl.getName()));
			if (type.isSimpleType()) {
				outputXSElementDecl(decl);
			} else {
				type.visit(this);
			}
			path.pop();
		}
	}

	public void modelGroupDecl(XSModelGroupDecl decl) {
		modelGroup(decl.getModelGroup());
	}

	public void modelGroup(XSModelGroup group) {
		modelGroup(group, "");
	}

	private void modelGroup(XSModelGroup group, String extraAtts) {
		final int len = group.getSize();
		for (int i = 0; i < len; i++)
			particle(group.getChild(i));
	}

	public void particle(XSParticle part) {
		// int i;

		StringBuffer buf = new StringBuffer();

		// i = part.getMaxOccurs();
		// if (i == XSParticle.UNBOUNDED)
		// buf.append(" maxOccurs=\"unbounded\"");
		// else if (i != 1)
		// buf.append(" maxOccurs=\"").append(i).append('\"');
		//
		// i = part.getMinOccurs();
		// if (i != 1)
		// buf.append(" minOccurs=\"").append(i).append('\"');

		final String extraAtts = buf.toString();

		part.getTerm().visit(new XSTermVisitor() {

			public void elementDecl(XSElementDecl decl) {
				if (decl.isLocal())
					SchemaVisitorWriter.this.elementDecl(decl, extraAtts);
				else {
				}
			}

			public void modelGroupDecl(XSModelGroupDecl decl) {
				// reference
			}

			public void modelGroup(XSModelGroup group) {
				SchemaVisitorWriter.this.modelGroup(group, extraAtts);
			}

			public void wildcard(XSWildcard wc) {
				SchemaVisitorWriter.this.wildcard("any", wc, extraAtts);
			}
		});
	}

	public void wildcard(XSWildcard wc) {
		wildcard("any", wc, "");
	}

	private void wildcard(String tagName, XSWildcard wc, String extraAtts) {
		switch (wc.getMode()) {
		case XSWildcard.LAX:
			break;
		case XSWildcard.STRTICT:
			break;
		case XSWildcard.SKIP:
			break;
		default:
			throw new AssertionError();
		}
	}

	private static final XSWildcardFunction<String> WILDCARD_NS = new XSWildcardFunction<String>() {

		public String any(Any wc) {
			return ""; // default
		}

		public String other(Other wc) {
			return " namespace='##other'";
		}

		public String union(Union wc) {
			StringBuffer buf = new StringBuffer(" namespace='");
			boolean first = true;
			for (String s : wc.getNamespaces()) {
				if (first)
					first = false;
				else
					buf.append(' ');
				buf.append(s);
			}
			return buf.append('\'').toString();
		}
	};

	public void annotation(XSAnnotation ann) {
	}

	public void identityConstraint(XSIdentityConstraint decl) {
	}

	public void xpath(XSXPath xp) {
	}

	public void empty(XSContentType t) {
	}

	public void facet(XSFacet facet) {
	}

	public void notation(XSNotation notation) {
	}

	private void outputXSComplexType(XSComplexType type) {
		try {
			out.write(explodeXPath());
			out.write(type.getName() + "\n");
			out.flush();
		} catch (Exception e) {
		}
	}

	private void outputXSAttributeDecl(XSAttributeDecl decl) {
		// XSSimpleType type = decl.getType();

		try {
			out.write(explodeXPath());
			out.write("@" + decl.getName() + "\n");
			out.flush();
		} catch (Exception e) {
		}
	}

	private String explodeXPath() {
		Iterator<PathComponent> pathIt = path.iterator();
		StringBuffer strbuf = new StringBuffer();
		while (pathIt.hasNext()) {
			strbuf.append(pathIt.next().getValue());
			strbuf.append("/");
		}
		return strbuf.toString();
	}

	private void outputXSElementDecl(XSElementDecl decl) {
		try {
			out.write(explodeXPath());
			out.write("@" + decl.getName() + "\n");
			out.flush();
		} catch (Exception e) {
		}

	}

}
