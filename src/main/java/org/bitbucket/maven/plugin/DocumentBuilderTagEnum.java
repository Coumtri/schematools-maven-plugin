package org.bitbucket.maven.plugin;

public enum DocumentBuilderTagEnum {
	ROOT("Paths"), ELEMENT("Path");

	private String tag;

	private DocumentBuilderTagEnum(String aTag) {
		this.tag = aTag;
	}

	public String getTag() {
		return this.tag;
	}
}
